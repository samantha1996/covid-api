require 'open-uri'
require 'nokogiri'
require 'json'
require 'jalalidate'

# This is a simple app to get stats of COVID-19 Confirmed cases, deaths and recoveries from Iran 

module IranCovid
    class Stats 
        def self.get_stats(uri="https://www.worldometers.info/coronavirus/country/iran/")
    
            regex_pattern = /([0-9])+\,+([0-9])+/
            document = Nokogiri::HTML(open(uri))

            events = document.css("#maincounter-wrap")
            events = events.text 
            events = events.split(' ')

            new_array = [] 
            events.each do |event|
                if event.match?(regex_pattern)
                    new_array.push(event)
                end 
            end 
            
            date = JalaliDate.new(Time.now())
            date = "#{date.year}/#{date.month}/#{date.day}" 
            
            json_dic = {"Date" => date, "Confirmed" => new_array[0], "Death" => new_array[1], "Recovered" => new_array[2]}

            return json_dic.to_json
        end 
    end 
end 
