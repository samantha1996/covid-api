FROM ruby:2.6.5

RUN mkdir /app
ADD . /app 
WORKDIR /app 

RUN bundle 

CMD ["ruby", "/app/covid-app.rb"] 